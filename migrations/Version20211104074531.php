<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211104074531 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blog_post (id VARCHAR(255) NOT NULL, title VARCHAR(50) NOT NULL, description VARCHAR(50) NOT NULL, content VARCHAR(500) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE champ (id INT AUTO_INCREMENT NOT NULL, onglet_id INT NOT NULL, name VARCHAR(50) NOT NULL, label VARCHAR(50) DEFAULT NULL, type VARCHAR(50) DEFAULT NULL, regex VARCHAR(50) DEFAULT NULL, class VARCHAR(50) DEFAULT NULL, ordre INT DEFAULT NULL, chplon INT DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, INDEX IDX_2F61E0ADBD1A86CC (onglet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, tree_root INT DEFAULT NULL, content_type_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, content_order INT DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, lft INT DEFAULT NULL, lvl INT DEFAULT NULL, rgt INT DEFAULT NULL, INDEX IDX_FEC530A9727ACA70 (parent_id), INDEX IDX_FEC530A9A977936C (tree_root), INDEX IDX_FEC530A91A445520 (content_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_family (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_type (id INT AUTO_INCREMENT NOT NULL, content_family_id INT DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, INDEX IDX_41BCBAECF3B91E69 (content_family_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE droits (id INT AUTO_INCREMENT NOT NULL, onglet_id INT DEFAULT NULL, user_id INT DEFAULT NULL, lecture TINYINT(1) DEFAULT NULL, modification TINYINT(1) DEFAULT NULL, creation TINYINT(1) DEFAULT NULL, supression TINYINT(1) DEFAULT NULL, INDEX IDX_7A9D4CEBD1A86CC (onglet_id), INDEX IDX_7A9D4CEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE editorial_model (id INT AUTO_INCREMENT NOT NULL, parent_model_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E0592B76E6B81B36 (parent_model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE editorial_model_content_type (editorial_model_id INT NOT NULL, content_type_id INT NOT NULL, INDEX IDX_5C44EA3A52FAC50B (editorial_model_id), INDEX IDX_5C44EA3A1A445520 (content_type_id), PRIMARY KEY(editorial_model_id, content_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field (id INT AUTO_INCREMENT NOT NULL, type_field_id INT DEFAULT NULL, content_type_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, label VARCHAR(255) DEFAULT NULL, field_order INT DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, INDEX IDX_5BF5455889C8204C (type_field_id), INDEX IDX_5BF545581A445520 (content_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id VARCHAR(255) NOT NULL, name VARCHAR(100) NOT NULL, path VARCHAR(100) NOT NULL, type VARCHAR(50) NOT NULL, size INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, label VARCHAR(50) NOT NULL, ordre INT DEFAULT NULL, class VARCHAR(50) DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, public TINYINT(1) DEFAULT NULL, url VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE onglet (id INT AUTO_INCREMENT NOT NULL, sousmenu_id INT DEFAULT NULL, onglet_type_id INT DEFAULT NULL, source_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, label VARCHAR(50) DEFAULT NULL, ordre INT DEFAULT NULL, path VARCHAR(50) DEFAULT NULL, class VARCHAR(50) DEFAULT NULL, visible TINYINT(1) DEFAULT NULL, public TINYINT(1) DEFAULT NULL, INDEX IDX_C6BC0239E5FBFC66 (sousmenu_id), INDEX IDX_C6BC0239B744FAE4 (onglet_type_id), INDEX IDX_C6BC0239953C1C61 (source_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE onglet_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE source (id INT AUTO_INCREMENT NOT NULL, typesource_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(50) DEFAULT NULL, url VARCHAR(70) DEFAULT NULL, tablename VARCHAR(50) DEFAULT NULL, apimethod VARCHAR(50) DEFAULT NULL, INDEX IDX_5F8A7F7376037EFD (typesource_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sous_menu (id INT AUTO_INCREMENT NOT NULL, menu_id INT NOT NULL, name VARCHAR(50) NOT NULL, label VARCHAR(50) DEFAULT NULL, ordre INT DEFAULT NULL, path VARCHAR(50) DEFAULT NULL, class VARCHAR(50) DEFAULT NULL, INDEX IDX_F30864DFCCD7E912 (menu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_field (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE typesource (id INT AUTO_INCREMENT NOT NULL, typesource_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(50) DEFAULT NULL, INDEX IDX_DBBE141876037EFD (typesource_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, groupe_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6497A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE champ ADD CONSTRAINT FK_2F61E0ADBD1A86CC FOREIGN KEY (onglet_id) REFERENCES onglet (id)');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A9727ACA70 FOREIGN KEY (parent_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A9A977936C FOREIGN KEY (tree_root) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A91A445520 FOREIGN KEY (content_type_id) REFERENCES content_type (id)');
        $this->addSql('ALTER TABLE content_type ADD CONSTRAINT FK_41BCBAECF3B91E69 FOREIGN KEY (content_family_id) REFERENCES content_family (id)');
        $this->addSql('ALTER TABLE droits ADD CONSTRAINT FK_7A9D4CEBD1A86CC FOREIGN KEY (onglet_id) REFERENCES onglet (id)');
        $this->addSql('ALTER TABLE droits ADD CONSTRAINT FK_7A9D4CEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE editorial_model ADD CONSTRAINT FK_E0592B76E6B81B36 FOREIGN KEY (parent_model_id) REFERENCES content_type (id)');
        $this->addSql('ALTER TABLE editorial_model_content_type ADD CONSTRAINT FK_5C44EA3A52FAC50B FOREIGN KEY (editorial_model_id) REFERENCES editorial_model (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE editorial_model_content_type ADD CONSTRAINT FK_5C44EA3A1A445520 FOREIGN KEY (content_type_id) REFERENCES content_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE field ADD CONSTRAINT FK_5BF5455889C8204C FOREIGN KEY (type_field_id) REFERENCES type_field (id)');
        $this->addSql('ALTER TABLE field ADD CONSTRAINT FK_5BF545581A445520 FOREIGN KEY (content_type_id) REFERENCES content_type (id)');
        $this->addSql('ALTER TABLE onglet ADD CONSTRAINT FK_C6BC0239E5FBFC66 FOREIGN KEY (sousmenu_id) REFERENCES sous_menu (id)');
        $this->addSql('ALTER TABLE onglet ADD CONSTRAINT FK_C6BC0239B744FAE4 FOREIGN KEY (onglet_type_id) REFERENCES onglet_type (id)');
        $this->addSql('ALTER TABLE onglet ADD CONSTRAINT FK_C6BC0239953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F7376037EFD FOREIGN KEY (typesource_id) REFERENCES typesource (id)');
        $this->addSql('ALTER TABLE sous_menu ADD CONSTRAINT FK_F30864DFCCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('ALTER TABLE typesource ADD CONSTRAINT FK_DBBE141876037EFD FOREIGN KEY (typesource_id) REFERENCES typesource (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A9727ACA70');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A9A977936C');
        $this->addSql('ALTER TABLE content_type DROP FOREIGN KEY FK_41BCBAECF3B91E69');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A91A445520');
        $this->addSql('ALTER TABLE editorial_model DROP FOREIGN KEY FK_E0592B76E6B81B36');
        $this->addSql('ALTER TABLE editorial_model_content_type DROP FOREIGN KEY FK_5C44EA3A1A445520');
        $this->addSql('ALTER TABLE field DROP FOREIGN KEY FK_5BF545581A445520');
        $this->addSql('ALTER TABLE editorial_model_content_type DROP FOREIGN KEY FK_5C44EA3A52FAC50B');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497A45358C');
        $this->addSql('ALTER TABLE sous_menu DROP FOREIGN KEY FK_F30864DFCCD7E912');
        $this->addSql('ALTER TABLE champ DROP FOREIGN KEY FK_2F61E0ADBD1A86CC');
        $this->addSql('ALTER TABLE droits DROP FOREIGN KEY FK_7A9D4CEBD1A86CC');
        $this->addSql('ALTER TABLE onglet DROP FOREIGN KEY FK_C6BC0239B744FAE4');
        $this->addSql('ALTER TABLE onglet DROP FOREIGN KEY FK_C6BC0239953C1C61');
        $this->addSql('ALTER TABLE onglet DROP FOREIGN KEY FK_C6BC0239E5FBFC66');
        $this->addSql('ALTER TABLE field DROP FOREIGN KEY FK_5BF5455889C8204C');
        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F7376037EFD');
        $this->addSql('ALTER TABLE typesource DROP FOREIGN KEY FK_DBBE141876037EFD');
        $this->addSql('ALTER TABLE droits DROP FOREIGN KEY FK_7A9D4CEA76ED395');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('DROP TABLE champ');
        $this->addSql('DROP TABLE content');
        $this->addSql('DROP TABLE content_family');
        $this->addSql('DROP TABLE content_type');
        $this->addSql('DROP TABLE droits');
        $this->addSql('DROP TABLE editorial_model');
        $this->addSql('DROP TABLE editorial_model_content_type');
        $this->addSql('DROP TABLE field');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE onglet');
        $this->addSql('DROP TABLE onglet_type');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE source');
        $this->addSql('DROP TABLE sous_menu');
        $this->addSql('DROP TABLE type_field');
        $this->addSql('DROP TABLE typesource');
        $this->addSql('DROP TABLE user');
    }
}
