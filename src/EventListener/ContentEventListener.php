<?php

namespace App\EventListener;

use App\Entity\Content;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ContentEventListener
{
    public function PrePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Content) {
            return;
        }
        if(null==$entity->getParent()){
        $entity->setRoot($entity->getId());
        $entity->setLvl(0);
        }else{
        $entity->setLvl($entity->getParent()->getLvl()+1);
        }

        $entityManager = $args->getObjectManager();
        $entityManager->flush(); 
    }

    public function PostPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Content) {
            return;
        }
        if(null==$entity->getParent()){
        $entity->setRoot($entity);
        }else{
        $entity->setRoot($entity->getParent()->getRoot());
        }

        $entityManager = $args->getObjectManager();
        $entityManager->flush(); 
    }
}