<?php

namespace App\Enum;

use Elao\Enum\ReadableEnum;
use Elao\Enum\ChoiceEnumTrait;

/**
 * @extends ReadableEnum<string> 
 */
final class ContentFamilyTypeEnum extends ReadableEnum
{
    /** @use ChoiceEnumTrait<string> */
    use ChoiceEnumTrait;

    public const CONTENT = 'content';
    public const INDEX = 'index';
    public const PERSON = 'person';

    public static function values(): array
    {
        return [
            self::CONTENT, 
            self::INDEX, 
            self::PERSON,
        ];
    }

    public static function choices(): array
    {
        return [
            self::CONTENT => 'content', 
            self::INDEX => 'index', 
            self::PERSON => 'person',
        ];
    }
}