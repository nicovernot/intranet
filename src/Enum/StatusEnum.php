<?php

namespace App\Enum;

use Elao\Enum\ReadableEnum;
use Elao\Enum\ChoiceEnumTrait;

/**
 * @extends ReadableEnum<string> 
 */
final class StatusEnum extends ReadableEnum
{
    /** @use ChoiceEnumTrait<string> */
    use ChoiceEnumTrait;

    public const DRAFT = 'draft';
    public const PUBLISHED = 'published';
    public const READY = 'ready';

    public static function values(): array
    {
        return [
            self::DRAFT, 
            self::PUBLISHED, 
            self::READY,
        ];
    }

    public static function choices(): array
    {
        return [
            self::DRAFT => 'draft', 
            self::PUBLISHED => 'published', 
            self::READY => 'ready',
        ];
    }
}