<?php
namespace App\Logic;
Interface Itemplate {
    public function setVariable(string $name,string $var);
    public function getHtml(string $template);

}