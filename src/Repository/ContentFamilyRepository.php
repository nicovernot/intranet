<?php

namespace App\Repository;

use App\Entity\ContentFamily;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContentFamily|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentFamily|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentFamily[]    findAll()
 * @method ContentFamily[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentFamilyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentFamily::class);
    }

    // /**
    //  * @return ContentFamily[] Returns an array of ContentFamily objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContentFamily
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
