<?php

namespace App\Repository;

use App\Entity\Typesource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Typesource|null find($id, $lockMode = null, $lockVersion = null)
 * @method Typesource|null findOneBy(array $criteria, array $orderBy = null)
 * @method Typesource[]    findAll()
 * @method Typesource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Typesource::class);
    }

    // /**
    //  * @return Typesource[] Returns an array of Typesource objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Typesource
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
