<?php

namespace App\Repository;

use App\Entity\OngletType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OngletType|null find($id, $lockMode = null, $lockVersion = null)
 * @method OngletType|null findOneBy(array $criteria, array $orderBy = null)
 * @method OngletType[]    findAll()
 * @method OngletType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OngletTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OngletType::class);
    }

    // /**
    //  * @return OngletType[] Returns an array of OngletType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OngletType
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
