<?php

namespace App\Repository;

use App\Entity\TypeField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeField|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeField|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeField[]    findAll()
 * @method TypeField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeField::class);
    }

    // /**
    //  * @return TypeField[] Returns an array of TypeField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeField
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
