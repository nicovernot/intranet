<?php

namespace App\Repository;

use App\Entity\EditorialModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EditorialModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method EditorialModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method EditorialModel[]    findAll()
 * @method EditorialModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditorialModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EditorialModel::class);
    }

    // /**
    //  * @return EditorialModel[] Returns an array of EditorialModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EditorialModel
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
