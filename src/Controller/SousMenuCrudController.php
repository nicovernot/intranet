<?php

namespace App\Controller;

use App\Entity\SousMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class SousMenuCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SousMenu::class;
    }

    public function configureFields(string $pageName): iterable
{
    return [
        Field::new('id')->hideOnForm(),
        Field::new('name'),
        Field::new('label'),
        Field::new('ordre'),
        Field::new('path'),
        Field::new('class'),
        AssociationField::new('menu')
        
    ];
}

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
