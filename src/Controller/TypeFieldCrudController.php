<?php

namespace App\Controller;

use App\Entity\TypeField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypeFieldCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypeField::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
