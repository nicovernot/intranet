<?php

namespace App\Controller;

use App\Entity\Champ;
use App\Entity\Onglet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Menu;
use App\Entity\SousMenu;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;
use League\Flysystem\FilesystemInterface;

class MenuController extends AbstractController
{
    private $client;
    private $logger;
    private $storage;

    public function __construct(HttpClientInterface $client,LoggerInterface $logger,FilesystemInterface $defaultStorage)
    {
        $this->storage = $defaultStorage;
        $this->client = $client;
        $this->logger = $logger;
    }
    public function sousmenu(Menu $menu)
    {
        $ssmenu = $this->getDoctrine()
            ->getRepository(SousMenu::class)
            ->findBy(["menu" => $menu]);
        return $ssmenu;
    }

    public function ongletSousmenu(SousMenu $sousMenu)
    {
        $onglets = $this->getDoctrine()
            ->getRepository(Onglet::class)
            ->findBy(["sousmenu" => $sousMenu]);
        return $onglets;
    }

    public function index()
    {
        $menus = $this->getDoctrine()
            ->getRepository(Menu::class)
        ->findAll();

        return $menus;
    }

    public function champs(int $id)
    {
        $champs = $this->getDoctrine()
            ->getRepository(Champ::class)
            ->findBy(["onglet" => $id]);
        return $champs;
    }

    public function  EntityProperties(string $name)
    {

        $class = $name;
        $em = $this->getDoctrine()->getManager();
        $meta = $em->getMetadataFactory()->getMetadataFor($class);
        return $meta;
    }

    public function getslqdqta(string $tablename) : array
    {
        $em = $this->getDoctrine()->getManager();

        $conn = $em->getConnection();
        if ($em->getConnection()->ping() === false) {
            $em->getConnection()->close();
            $conn->connect();
        }
        $sql = '
        SELECT * FROM '.$tablename.' p
       
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function httpClient(){
        $response = $this->client->request(
            'GET',
            'http://192.168.1.10:1338/bog-posts'
        );
        $content = $response->toArray();

      //  $this->storage->write("test.json",json_encode($content) );
    return array_keys($content[0]);
    }
}
