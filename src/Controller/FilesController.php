<?php

namespace App\Controller;

use App\Document\Book;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Finder\Finder;
use App\Document\Product;
use App\Document\Chapitre;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;


class FilesController extends AbstractController
{

    public function __construct(LoggerInterface $fileLogger)
    {
        $this->logger = $fileLogger;
    }
    /**
     * @Route("/files", name="files")
     */
    public function index(Request $request,FilesystemInterface $defaultStorage)
    {
       $content ="";
     //  $defaultStorage->write('testdir/','',[]);

       $filename = $request->query->get('name');
           // comment
       $filesinrep = [];
       $reps = $this->rootRepList();
        $this->logger->info(serialize($reps));
        foreach ($reps as $file) {
            //$contents = $file->getContents();
           $repname =  $file->getFilename();
           $filesinrep[$repname] = $this->repdetail($repname);

            foreach ($this->repdetail($repname) as $mfile) {
                $filename1 = $mfile->getFilename();
                if($filename==$filename1) $content = $mfile->getContents();

            }
        }


       return $this->render('files/index.html.twig', [
            'controller_name' => 'FilesController',
            'reps' => $reps,
            'filesinrep' => $filesinrep,
            'content' => $content
        ]);
    }

    /**
     * @Route("/files/{page}", name="filesdetail")
     */
    public function fdetail(string $page )
    {
        $finder = new Finder();
        $reps = $finder->files()->in('public/build/html/'.$page);



        return $this->render('files/index.html.twig', [
            'controller_name' => 'FilesController',

            'reps' => $reps
        ]);
    }


    public function content(string $page )
    {
        $finder = new Finder();
        $content = $finder->files()->name($page);


        return $content;
    }

    /**
     * @Route("/createproduct", name="createproduct")
     */
    public function createAction(DocumentManager $dm)
    {


        $product = new Chapitre();
        $product->setTitre('produit3');
        $product->setTexte('texte3');

        $dm->persist($product);
        $dm->flush();

        return new Response('Created product id ' . $product->getId());
    }
    /**
     * @Route("/showproduct", name="showproduct")
     */
    public function showAction(DocumentManager $dm)
    {
        $products = $dm->getRepository(Book::class)->findAll();

        if (! $products) {
            throw $this->createNotFoundException('No product found for id ' );
        }

        return $this->render('document/index.html.twig',[
            'products'=> $products
            ]);
    }

    /**
     * @Route("/showproductid/{id<\d+>}", name="showproductid")
     */
    public function showIdAction(DocumentManager $dm,$id)
    {
        $product = $dm->getRepository(Product::class)->find($id);

        if (! $product) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }
        var_dump($product);
        return new Response('Created product id ' . $product->getId());
    }

    public function rootRepList()
    {
        $finder = new Finder();
        $reps = $finder->directories()->in($this->getParameter('kernel.project_dir').'/public/build/html/');



        return $reps;
    }

    public function repdetail(string $page )
    {
        $finder = new Finder();
        $fileList = $finder->files()->in($this->getParameter('kernel.project_dir').'/public/build/html/'.$page);
        return $fileList;
    }
}
