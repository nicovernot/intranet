<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use App\Entity\Champ;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class ChampCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Champ::class;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('onglet')
            ->add('name')
            ->add('visible')
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('id')->hideOnForm(),
            Field::new('name'),
            Field::new('label'),
            Field::new('ordre'),
            Field::new('type'),
            Field::new('class'),
            Field::new('chplon'),
            Field::new('visible'),
            AssociationField::new('onglet')
            
        ];
    }
    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
