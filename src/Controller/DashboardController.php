<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Menu;
use App\Entity\SousMenu;
use App\Entity\Onglet;
use App\Entity\Champ;
use App\Entity\Groupe;
use App\Entity\Droits;
use App\Entity\OngletType;
use App\Entity\Source;
use App\Entity\Typesource;
use App\Entity\Content;
use App\Entity\ContentType;
use App\Entity\Field;
use App\Entity\TypeField;
use App\Entity\ContentFamily;
use App\Entity\EditorialModel;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin1", name="admin")
     */
    public function index(): Response
    {
        return $this->render('dash.html.twig');

        
    }



    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
        ->setTitle('<img src="/images/openeditions.png"/> ');
    }

    public function configureMenuItems(): iterable
    {   
        yield MenuItem::linkToUrl('Accueil', 'fa fa-home', '/');
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-pen');
        yield MenuItem::section('Users');
        yield MenuItem::linkToCrud('Users', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('Groupe', 'fas fa-user-friends', Groupe::class);
        yield MenuItem::linkToCrud('Droits', 'far fa-thumbs-up', Droits::class);
        yield MenuItem::section('Admin');
        yield MenuItem::linkToCrud('Menus', 'fas fa-th', Menu::class);
        yield MenuItem::linkToCrud('Sous Menus', 'fas fa-th', SousMenu::class);
        yield MenuItem::linkToCrud('Onglets', 'fas fa-th', Onglet::class);
        yield MenuItem::linkToCrud('Champs', 'fas fa-th', Champ::class);
        yield MenuItem::linkToCrud('Type Onglet', 'fas fa-th', OngletType::class);
        yield MenuItem::linkToCrud('Source', 'fas fa-th', Source::class);
        yield MenuItem::linkToCrud('Type Source', 'fas fa-th', Typesource::class);
        yield MenuItem::section('Admin');
        yield MenuItem::linkToCrud('Content', 'fas fa-th', Content::class);
        yield MenuItem::linkToCrud('Content Type', 'fas fa-th', ContentType::class);
        yield MenuItem::linkToCrud('Content Family', 'fas fa-th', ContentFamily::class);
        yield MenuItem::linkToCrud('Field', 'fas fa-th', Field::class);
        yield MenuItem::linkToCrud('Field Type', 'fas fa-th', TypeField::class);
        yield MenuItem::linkToCrud('Editorial Model', 'fas fa-th', EditorialModel::class);
    }
}
