<?php

namespace App\Controller;

use App\Entity\ContentType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class ContentTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ContentType::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('label'),
            Field::new('name'),
            AssociationField::new('contentFamily'),
            AssociationField::new('editorialModels'),
        ];
    }
    
}
