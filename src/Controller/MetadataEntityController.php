<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class MetadataEntityController extends AbstractController
{
    /**
     * @Route("/metadata/entity", name="metadata_entity")
     */
    public function index()
    {

        $em = $this->getDoctrine()->getManager();
    
        $className = "App\Entity\Onglet";
    
        $metadata = $em->getClassMetadata($className);

        return $this->render('metadata_entity/index.html.twig', [
            'controller_name' => 'MetadataEntityController',
            'meta' => $metadata->fieldMappings,
        ]);
    }
}
