<?php

namespace App\Controller;

use App\Entity\Content;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;

class ContentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Content::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
           
            Field::new('name'),
            Field::new('label'),
            Field::new('contentOrder'),
            Field::new('visible'),
            Field::new('lft'),
            Field::new('lvl'),
            Field::new('rgt'),
            AssociationField::new('parent'),
            AssociationField::new('contentType'),
            AssociationField::new('parent'),
            AssociationField::new('root'),
            AssociationField::new('children'),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('label')
            ->add('visible')
        ;
    }

}
