<?php

namespace App\Controller;

use App\Entity\EditorialModel;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class EditorialModelCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EditorialModel::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('name'),
            Field::new('label'),
            AssociationField::new('parentModel'),
            AssociationField::new('childrenModels'),
        ];
    }

}
