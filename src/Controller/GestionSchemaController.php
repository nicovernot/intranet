<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpFoundation\Request;

class GestionSchemaController extends AbstractController
{
    /**
     * @Route("/gschema", name="gestion_schema")
     */
    public function index(Connection $connection,Request $request)
    {
       
        $nomtable= $request->query->get('nmtbl');
        $ong= $request->query->get("nglt");
 
        if(isset($ong) and isset($nomtable)){
           $sql1 = "SELECT * 
           FROM `INFORMATION_SCHEMA`.`COLUMNS` 
           WHERE `TABLE_NAME` = '$nomtable';" ;
     
         $cols = $connection->fetchAll($sql1);
       
       foreach ($cols as $key => $value) {
       // $seq = $connection->fetchAll("select nextval('champ_id_seq');");
       // $seq = $seq[0]["nextval"];
       if ($value ["CHARACTER_MAXIMUM_LENGTH"]>350){
        $value ["CHARACTER_MAXIMUM_LENGTH"]=350;
       }
         $connection->insert('champ', array('onglet_id' => $ong,'name'=>$value["COLUMN_NAME"],'label'=>$value["COLUMN_NAME"],'ordre'=>$value ["ORDINAL_POSITION"],'type'=>$value ["DATA_TYPE"],'chplon'=>$value ["CHARACTER_MAXIMUM_LENGTH"],'visible'=>1));
       }  
        }
       
            
            
         $entity = "App\Entity\Onglet";
           $onglets = $this->getDoctrine()
        ->getRepository($entity)
        ->findAll();

      $sm = $connection->getSchemaManager();
      //$databases = $sm->listDatabases();
      $tables = $sm->listTables();
     // $tables = "tables"; 
       return $this->render('gestion_schema/index.html.twig', [
            'controller_name' => 'GestionSchemaController',
            'tables' => $tables,
           
           'onglets' => $onglets,
        ]);
    }
}