<?php

namespace App\Controller;

use App\Entity\Typesource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypesourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Typesource::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
