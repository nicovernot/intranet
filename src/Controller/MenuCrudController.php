<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\SousMenuFormType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class MenuCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Menu::class;
    }




    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('id')->hideOnForm(),
            Field::new('name'),
            Field::new('label'),
            Field::new('ordre'),
            Field::new('url'),
            Field::new('visible'),
            Field::new('public'),
            
            
        ];
    }


    /*


    public function configureFields(string $pageName): iterable
         {
            return [
                 TextField::new('name'),
                 
                  CollectionField::new('SousMenu')
                       ->SetEntryType(SousMenu::class)
            ];
         }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
