<?php

namespace App\Controller;

use App\Entity\Onglet;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class OngletCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Onglet::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('id')->hideOnForm(),
            Field::new('name'),
            Field::new('label'),
            Field::new('ordre'),
            Field::new('path'),
            Field::new('class'),
            Field::new('visible'),
            Field::new('public'),
            AssociationField::new('ongletType'),
            AssociationField::new('sousmenu'),
            AssociationField::new('source')
        ];
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
