<?php

namespace App\Controller;

use App\Entity\OngletType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class OngletTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OngletType::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
