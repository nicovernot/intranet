<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Champ;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {


        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/datatables", name="dt")
     */
    public function showAction(Request $request, DataTableFactory $dataTableFactory)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('App\Entity\Content');
        $tree = $repo->createQueryBuilder('node')->getQuery()
        ->setHint(\Doctrine\ORM\Query::HINT_INCLUDE_META_COLUMNS, true)
        ->getResult();
        dd($tree);

        $table = $dataTableFactory->create()
        ->add('name', TextColumn::class)
        ->add('label', TextColumn::class)
            ->createAdapter(ORMAdapter::class, [
                'entity' => Champ::class,
            ])
            ->handleRequest($request);

        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('list.html.twig', ['datatable' => $table]);
    }
}
