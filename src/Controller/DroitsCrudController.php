<?php

namespace App\Controller;

use App\Entity\Droits;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class DroitsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Droits::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('id')->hideOnForm(),
            Field::new('lecture'),
            Field::new('supression'),
            Field::new('creation'),
            Field::new('modification'),
            AssociationField::new('onglet'),
            AssociationField::new('user'),
        ];
    }


    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
