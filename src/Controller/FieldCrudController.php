<?php

namespace App\Controller;

use App\Entity\Field;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field as EaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FieldCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Field::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
           
            EaField::new('name'),
            EaField::new('label'),
            EaField::new('fieldOrder'),
            EaField::new('visible'),
            AssociationField::new('typeField'),
            AssociationField::new('contentType'),
        ];
    }
    
}
