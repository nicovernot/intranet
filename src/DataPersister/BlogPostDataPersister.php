<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\BlogPost;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class BlogPostDataPersister implements ContextAwareDataPersisterInterface
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof BlogPost;
    }

    public function persist($data, array $context = [])
    {
        $response = $this->client->request(
            'POST',
            'http://192.168.1.10:1338/bog-posts',
           ['body'=>['title'=>$data->getTitle(),'description'=>$data->getDescription(),'content'=>$data->getContent()]]
        );

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $data = $response->toArray();

        return $data;
    }

    public function remove($data, array $context = [])
    {
        // call your persistence layer to delete $data
    }
}