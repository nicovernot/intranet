<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContentTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ContentTypeRepository::class)
 */
class ContentType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity=Field::class, mappedBy="contentType")
     */
    private $fields;

    /**
     * @ORM\ManyToOne(targetEntity=ContentFamily::class, inversedBy="contentTypes")
     */
    private $contentFamily;

    /**
     * @ORM\OneToMany(targetEntity=Content::class, mappedBy="contentType")
     */
    private $contents;

    /**
     * @ORM\ManyToMany(targetEntity=EditorialModel::class, mappedBy="childrenModels")
     */
    private $editorialModels;

    /**
     * @ORM\ManyToMany(targetEntity=ContentType::class, inversedBy="relatedContentTypes")
     */
    private $possibleParents;

    /**
     * @ORM\ManyToMany(targetEntity=ContentType::class, mappedBy="possibleParents")
     */
    private $relatedContentTypes;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->contents = new ArrayCollection();
        $this->editorialModels = new ArrayCollection();
        $this->possibleParents = new ArrayCollection();
        $this->relatedContentTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
   

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(Field $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setContentType($this);
        }

        return $this;
    }

    public function removeField(Field $field): self
    {
        if ($this->fields->removeElement($field)) {
            // set the owning side to null (unless already changed)
            if ($field->getContentType() === $this) {
                $field->setContentType(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
       return $this->getName();
    }

    public function getContentFamily(): ?ContentFamily
    {
        return $this->contentFamily;
    }

    public function setContentFamily(?ContentFamily $contentFamily): self
    {
        $this->contentFamily = $contentFamily;

        return $this;
    }

   
    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setContentType($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->removeElement($content)) {
            // set the owning side to null (unless already changed)
            if ($content->getContentType() === $this) {
                $content->setContentType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EditorialModel[]
     */
    public function getEditorialModels(): Collection
    {
        return $this->editorialModels;
    }

    public function addEditorialModel(EditorialModel $editorialModel): self
    {
        if (!$this->editorialModels->contains($editorialModel)) {
            $this->editorialModels[] = $editorialModel;
            $editorialModel->addChildrenModel($this);
        }

        return $this;
    }

    public function removeEditorialModel(EditorialModel $editorialModel): self
    {
        if ($this->editorialModels->removeElement($editorialModel)) {
            $editorialModel->removeChildrenModel($this);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getPossibleParents(): Collection
    {
        return $this->possibleParents;
    }

    public function addPossibleParent(self $possibleParent): self
    {
        if (!$this->possibleParents->contains($possibleParent)) {
            $this->possibleParents[] = $possibleParent;
        }

        return $this;
    }

    public function removePossibleParent(self $possibleParent): self
    {
        $this->possibleParents->removeElement($possibleParent);

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getRelatedContentTypes(): Collection
    {
        return $this->relatedContentTypes;
    }

    public function addRelatedContentType(self $relatedContentType): self
    {
        if (!$this->relatedContentTypes->contains($relatedContentType)) {
            $this->relatedContentTypes[] = $relatedContentType;
            $relatedContentType->addPossibleParent($this);
        }

        return $this;
    }

    public function removeRelatedContentType(self $relatedContentType): self
    {
        if ($this->relatedContentTypes->removeElement($relatedContentType)) {
            $relatedContentType->removePossibleParent($this);
        }

        return $this;
    }
}
