<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SourceRepository::class)
 */
class Source
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Onglet::class, mappedBy="source")
     */
    private $onglet;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tablename;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $apimethod;

    /**
     * @ORM\ManyToOne(targetEntity=Typesource::class, inversedBy="sources")
     */
    private $typesource;

    public function __construct()
    {
        $this->onglet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Onglet[]
     */
    public function getOnglet(): Collection
    {
        return $this->onglet;
    }

    public function addOnglet(Onglet $onglet): self
    {
        if (!$this->onglet->contains($onglet)) {
            $this->onglet[] = $onglet;
            $onglet->setSource($this);
        }

        return $this;
    }

    public function removeOnglet(Onglet $onglet): self
    {
        if ($this->onglet->contains($onglet)) {
            $this->onglet->removeElement($onglet);
            // set the owning side to null (unless already changed)
            if ($onglet->getSource() === $this) {
                $onglet->setSource(null);
            }
        }

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTablename(): ?string
    {
        return $this->tablename;
    }

    public function setTablename(?string $tablename): self
    {
        $this->tablename = $tablename;

        return $this;
    }

    public function getApimethod(): ?string
    {
        return $this->apimethod;
    }

    public function setApimethod(?string $apimethod): self
    {
        $this->apimethod = $apimethod;

        return $this;
    }

    public function __toString()
    {
       return $this->getName();
    }

    public function getTypesource(): ?Typesource
    {
        return $this->typesource;
    }

    public function setTypesource(?Typesource $typesource): self
    {
        $this->typesource = $typesource;

        return $this;
    }

}
