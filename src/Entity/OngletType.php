<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OngletTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=OngletTypeRepository::class)
 */
class OngletType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Onglet::class, mappedBy="ongletType")
     */
    private $onglet;

    public function __construct()
    {
        $this->onglet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Onglet[]
     */
    public function getOnglet(): Collection
    {
        return $this->onglet;
    }

    public function addOnglet(Onglet $onglet): self
    {
        if (!$this->onglet->contains($onglet)) {
            $this->onglet[] = $onglet;
            $onglet->setOngletType($this);
        }

        return $this;
    }

    public function removeOnglet(Onglet $onglet): self
    {
        if ($this->onglet->contains($onglet)) {
            $this->onglet->removeElement($onglet);
            // set the owning side to null (unless already changed)
            if ($onglet->getOngletType() === $this) {
                $onglet->setOngletType(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
       return $this->getName();
    }
}
