<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypesourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TypesourceRepository::class)
 */
class Typesource
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Typesource::class, inversedBy="source")
     */
    private $typesource;

    /**
     * @ORM\OneToMany(targetEntity=Typesource::class, mappedBy="typesource")
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity=Source::class, mappedBy="typesource")
     */
    private $sources;

    public function __construct()
    {
        $this->source = new ArrayCollection();
        $this->sources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTypesource(): ?self
    {
        return $this->typesource;
    }

    public function setTypesource(?self $typesource): self
    {
        $this->typesource = $typesource;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSource(): Collection
    {
        return $this->source;
    }

    public function addSource(self $source): self
    {
        if (!$this->source->contains($source)) {
            $this->source[] = $source;
            $source->setTypesource($this);
        }

        return $this;
    }

    public function removeSource(self $source): self
    {
        if ($this->source->contains($source)) {
            $this->source->removeElement($source);
            // set the owning side to null (unless already changed)
            if ($source->getTypesource() === $this) {
                $source->setTypesource(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Source[]
     */
    public function getSources(): Collection
    {
        return $this->sources;
    }

    public function __toString()
    {
    return $this->getName();
    }

}
