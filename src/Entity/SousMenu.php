<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SousMenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=SousMenuRepository::class)
 */
class SousMenu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $class;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="sousMenus",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $menu;

    /**
     * @ORM\OneToMany(targetEntity=Onglet::class, mappedBy="sousmenu")
     */
    private $onglets;

    public function __construct()
    {
        $this->onglets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }


    /**
     * @return Collection|Onglet[]
     */
    public function getOnglets(): Collection
    {
        return $this->onglets;
    }

    public function addOnglet(Onglet $onglet): self
    {
        if (!$this->onglets->contains($onglet)) {
            $this->onglets[] = $onglet;
            $onglet->setSousmenu($this);
        }

        return $this;
    }

    public function removeOnglet(Onglet $onglet): self
    {
        if ($this->onglets->contains($onglet)) {
            $this->onglets->removeElement($onglet);
            // set the owning side to null (unless already changed)
            if ($onglet->getSousmenu() === $this) {
                $onglet->setSousmenu(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
       return $this->getMenu().'-'.$this->getName();
    }
}
