<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FieldTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=FieldTypeRepository::class)
 */
class FieldType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Field::class, mappedBy="fieldType")
     */
    private $field;

    public function __construct()
    {
        $this->field = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Field[]
     */
    public function getField(): Collection
    {
        return $this->field;
    }

    public function addField(Field $field): self
    {
        if (!$this->field->contains($field)) {
            $this->field[] = $field;
            $field->setFieldType($this);
        }

        return $this;
    }

    public function removeField(Field $field): self
    {
        if ($this->field->removeElement($field)) {
            // set the owning side to null (unless already changed)
            if ($field->getFieldType() === $this) {
                $field->setFieldType(null);
            }
        }

        return $this;
    }
    
    public function __toString()
    {
       return $this->getName();
    }
}
