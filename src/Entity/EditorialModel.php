<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EditorialModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=EditorialModelRepository::class)
 */
class EditorialModel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToOne(targetEntity=ContentType::class, cascade={"persist", "remove"})
     */
    private $parentModel;

    /**
     * @ORM\ManyToMany(targetEntity=ContentType::class, inversedBy="editorialModels")
     */
    private $childrenModels;



    public function __construct()
    {
        $this->childrenModels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getParentModel(): ?ContentType
    {
        return $this->parentModel;
    }

    public function setParentModel(?ContentType $parentModel): self
    {
        $this->parentModel = $parentModel;

        return $this;
    }

   
    public function __toString()
    {
       return $this->getName();
    }

    /**
     * @return Collection|ContentType[]
     */
    public function getChildrenModels(): Collection
    {
        return $this->childrenModels;
    }

    public function addChildrenModel(ContentType $childrenModel): self
    {
        if (!$this->childrenModels->contains($childrenModel)) {
            $this->childrenModels[] = $childrenModel;
        }

        return $this;
    }

    public function removeChildrenModel(ContentType $childrenModel): self
    {
        $this->childrenModels->removeElement($childrenModel);

        return $this;
    }
}
