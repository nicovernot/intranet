<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DroitsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=DroitsRepository::class)
 */
class Droits
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Onglet::class, inversedBy="droits")
     */
    private $onglet;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $lecture;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $modification;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $creation;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $supression;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="droits")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOnglet(): ?Onglet
    {
        return $this->onglet;
    }

    public function setOnglet(?Onglet $onglet): self
    {
        $this->onglet = $onglet;

        return $this;
    }

    public function getLecture(): ?bool
    {
        return $this->lecture;
    }

    public function setLecture(?bool $lecture): self
    {
        $this->lecture = $lecture;

        return $this;
    }

    public function getModification(): ?bool
    {
        return $this->modification;
    }

    public function setModification(?bool $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getCreation(): ?bool
    {
        return $this->creation;
    }

    public function setCreation(?bool $creation): self
    {
        $this->creation = $creation;

        return $this;
    }

    public function getSupression(): ?bool
    {
        return $this->supression;
    }

    public function setSupression(?bool $supression): self
    {
        $this->supression = $supression;

        return $this;
    }
    public function __toString()
    {
       return $this->getName();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
