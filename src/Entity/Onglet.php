<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OngletRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ApiResource( )
 * @ORM\Entity(repositoryClass=OngletRepository::class)
 */
class Onglet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity=SousMenu::class, inversedBy="onglets")
     */
    private $sousmenu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $class;

    /**
     * @ORM\OneToMany(targetEntity=Champ::class, mappedBy="onglet")
     * @ApiSubresource()
     */
    private $champs;

    /**
     * @ORM\OneToMany(targetEntity=Droits::class, mappedBy="onglet")
     */
    private $droits;

    /**
     * @ORM\ManyToOne(targetEntity=OngletType::class, inversedBy="onglet")
     */
    private $ongletType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $public;

    /**
     * @ORM\ManyToOne(targetEntity=Source::class, inversedBy="onglet")
     */
    private $source;

    public function __construct()
    {
        $this->champs = new ArrayCollection();
        $this->droits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getSousmenu(): ?SousMenu
    {
        return $this->sousmenu;
    }

    public function setSousmenu(?SousMenu $sousmenu): self
    {
        $this->sousmenu = $sousmenu;

        return $this;
    }


    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): self
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return Collection|Champ[]
     */
    public function getChamps(): Collection
    {
        return $this->champs;
    }

    public function addChamp(Champ $champ): self
    {
        if (!$this->champs->contains($champ)) {
            $this->champs[] = $champ;
            $champ->setOnglet($this);
        }

        return $this;
    }

    public function removeChamp(Champ $champ): self
    {
        if ($this->champs->contains($champ)) {
            $this->champs->removeElement($champ);
            // set the owning side to null (unless already changed)
            if ($champ->getOnglet() === $this) {
                $champ->setOnglet(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
       return $this->getSousmenu().'-'.$this->getName();
    }

    /**
     * @return Collection|Droits[]
     */
    public function getDroits(): Collection
    {
        return $this->droits;
    }

    public function addDroit(Droits $droit): self
    {
        if (!$this->droits->contains($droit)) {
            $this->droits[] = $droit;
            $droit->setOnglet($this);
        }

        return $this;
    }

    public function removeDroit(Droits $droit): self
    {
        if ($this->droits->contains($droit)) {
            $this->droits->removeElement($droit);
            // set the owning side to null (unless already changed)
            if ($droit->getOnglet() === $this) {
                $droit->setOnglet(null);
            }
        }

        return $this;
    }

    public function getOngletType(): ?OngletType
    {
        return $this->ongletType;
    }

    public function setOngletType(?OngletType $ongletType): self
    {
        $this->ongletType = $ongletType;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(?bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }
}
