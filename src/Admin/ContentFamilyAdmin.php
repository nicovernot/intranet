<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Enum\ContentFamilyTypeEnum;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;

final class ContentFamilyAdmin extends AbstractAdmin
{

    protected function configureTabMenu(MenuItemInterface $menu, string $action, ?AdminInterface $childAdmin = null): void
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        $childId =  $admin->getRequest()->get('childId');
        $menu->addChild('View Content Family', $admin->generateMenuUrl('show', ['id' => $id]));

        if ($this->isGranted('EDIT')) {
            $menu->addChild('Edit Content Family', $admin->generateMenuUrl('edit', ['id' => $id]));
        }
        if ($this->isGranted('LIST')) {

            $menu->addChild('Manage Content Types', $admin->generateMenuUrl('admin.content_type.list', ['id' => $id,'childId' => $childId]));
            if(!empty($admin->getRequest()->get('childId'))){
                $menu->addChild( 'Manage Fields',
                    [
                        'uri' => $admin->generateUrl('admin.content_type|admin.field.list', [
                            'id' => $id, 
                            'childId' => $admin->getRequest()->get('childId')
                             
                        ])
                    ]);
            }
        }
        
    }
    
    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('label')
            ->add('name')
            ->add('type')
            ->add('contentTypes')
            ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('label')
            ->add('name')
            ->add('type')
            ->add('contentTypes')
            ->add(ListMapper::NAME_ACTIONS, null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('label')
            ->add('name')
            ->add('type',ChoiceType::class,array('choices' => ContentFamilyTypeEnum::readables()))
            ->add('contentTypes')
            ;
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('label')
            ->add('name')
            ->add('contentTypes')
            ;
    }
}
