<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Enum\StatusEnum;
use Sonata\AdminBundle\Route\RouteCollectionInterface;

final class ContentAdmin extends AbstractAdmin
{   

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        if ($this->isChild()) {
            return;
        }

        // This is the route configuration as a parent
        $collection->clear();

    }
    
    protected function configureDatagridFilters(DatagridMapper $filter): void
    {
        $filter
            ->add('site')
            ->add('name')
            ->add('label')
            ->add('contentOrder')
            ->add('visible')
            ->add('lvl')
            ->add('root')
            ->add('parent')
            ->add('children')
            ->add('status')
            ;
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('site')
            ->add('name')
            ->add('label')
            ->add('contentOrder')
            ->add('visible')
            ->add('lvl')
            ->add('contentType')
            ->add('root')
            ->add('parent')
            ->add('children')
            ->add('status')
            ->add(ListMapper::NAME_ACTIONS, null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('site')
            ->add('name')
            ->add('label')
            ->add('contentOrder')
            ->add('visible')
            ->add('lvl')
            ->add('root')
            ->add('parent')
            ->add('children')
            ->add('contentType')
            ->add('status',ChoiceType::class,
            array('choices' => StatusEnum::readables()));
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('site')
            ->add('name')
            ->add('label')
            ->add('contentOrder')
            ->add('visible')
            ->add('lvl')
            ->add('status')
            ;
    }
}
