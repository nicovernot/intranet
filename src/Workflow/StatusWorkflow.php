<?php

namespace App\Workflow;

use Symfony\Component\Workflow\Registry;
use App\Entity\Content;

final class StatusWorkflow
{
    private $workflows;

    public function __construct(Registry $workflows)
    {
        $this->workflows = $workflows;
    }

    public function updateStatus(Content $content)
    {
        $stateMachine = $this->workflows->get($content, 'content_status');
        if ($stateMachine->can($content->status == "draft")) {
            $stateMachine->apply($content, 'ready');
        } elseif ($stateMachine->can($content->status == "ready")) {
            $stateMachine->apply($content, 'published');  
        }elseif($stateMachine->can($content->status == "published")){
            $stateMachine->apply($content, 'ready'); 
        }

    }




}