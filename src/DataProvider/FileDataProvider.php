<?php
namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\File;
use League\Flysystem\FilesystemInterface;

final class FileDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{

    private $storage;

    // The variable name $defaultStorage matters: it needs to be the camelized version
    // of the name of your storage.
    public function __construct(FilesystemInterface $defaultStorage)
    {
        $this->storage = $defaultStorage;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return File::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $filelist = $this->storage->listContents();
        foreach ($filelist as $key=>$data) {
            $bl = new File();
            $bl ->setId(md5($data['timestamp']));
            $bl->setName($data['filename']);
            $bl->setPath($data['path']);
            $bl->setType($data['type']);
            $bl->setSize($data['timestamp']);
           // $bl->setSize($data['size']);
            yield $bl;
        }

    }
}