<?php
// api/src/DataProvider/BlogPostItemDataProvider.php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\BlogPost;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

final class BlogPostItemDataProvider implements ItemDataProviderInterface, SerializerAwareDataProviderInterface
{
    private $client;
    private $logger;
    public function __construct(HttpClientInterface $client,LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    use SerializerAwareDataProviderTrait;

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return BlogPost::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?BlogPost
    {
        // Retrieve data from anywhere you want, in a custom format
        $response = $this->client->request(
            'GET',
            "http://192.168.1.10:1338/bog-posts/$id"
        );

        $content = $response->toArray();

        $data = json_encode($content);
        $this->logger->info(json_encode($data));
        // Deserialize data using the Serializer
        return $this->getSerializer()->deserialize($data, BlogPost::class, 'json');
    }
}