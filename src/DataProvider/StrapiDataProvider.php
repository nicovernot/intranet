<?php
namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\BlogPost;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class StrapiDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return BlogPost::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $response = $this->client->request(
            'GET',
            'http://192.168.1.10:1338/bog-posts'
        );

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
       // $content = $response->getContent();
        $content = $response->toArray();
        foreach ($content as $key=>$data) {
            $bl = new BlogPost();
            $bl ->setId($data['id']);
            $bl->setTitle($data['title']);
            $bl->setContent($data['content']);
           yield $bl;
        }

    }
}